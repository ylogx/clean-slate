#!/usr/bin/env bash
set -eu

read -p "The 'Clean Slate Protocol' Sir! Please confirm (y/n)? " answer
case ${answer:0:1} in
  y|Y )
    echo "Christmas it is."
  ;;
  * )
    exit 0
  ;;
esac
echo ""

ansible-playbook -u chaudhary --private-key ~/.ssh/id_ed25519 protocol_playbook.yml
