Clean Slate Protocol
====================

> "J.A.R.V.I.S., hey."
>
> "All wrapped up here, sir. Will there be anything else?"
>
> "You know what to do."
>
> "The "Clean Slate" Protocol, sir?"
>
> "**Screw it, it's Christmas.** Yes, yes!"

\- [Tony Stark and J.A.R.V.I.S.][clean-slate]

[clean-slate]: http://marvelcinematicuniverse.wikia.com/wiki/Clean_Slate_Protocol
