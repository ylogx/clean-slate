#!/usr/bin/env bash
set -eu

pip2 install ansible
echo "Dependencies installed."

if [[ ! -f ansible_hosts.ini ]]; then
  echo "Please make sure to fill the ansible_hosts.ini file!"
fi
