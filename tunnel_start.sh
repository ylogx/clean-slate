#!/usr/bin/env zsh
tmux send-keys -t "0:2.0" C-c 'echo "Tunnels: $(date)"' Enter
#tmux send-keys -t "0:2.1" C-c 'sshba 8157 entry-zanalytics' Enter
tmux send-keys -t "0:2.2" C-c 'sshba 8160 gcloud-rpi' Enter
tmux send-keys -t "0:2.3" C-c 'sshba 8170 spider' Enter
#tmux send-keys -t "0:2.4" C-c 'sshba 8180 recsys' Enter
tmux send-keys -t "0:2.4" C-c 'sshba 8180 10.0.240.16' Enter
